#include "stdafx.h"
#include <Poco/Net/HTTPClientSession.h>
#include <Poco/Net/HTTPRequest.h>
#include <Poco/Net/HTTPResponse.h>
#include <Poco/StreamCopier.h>
#include <Poco/Path.h>
#include <Poco/URI.h>
#include <Poco/Exception.h>
#include <iostream>
#include <string>

using namespace Poco::Net;
using namespace Poco;
using namespace std;

bool doRequest(Poco::Net::HTTPClientSession& session, Poco::Net::HTTPRequest& request, Poco::Net::HTTPResponse& response)
{
	try
	{
		session.sendRequest(request);
		std::istream& rs = session.receiveResponse(response);
		std::cout << response.getStatus() << " " << response.getReason() << std::endl;
		if (response.getStatus() != Poco::Net::HTTPResponse::HTTP_UNAUTHORIZED)
		{
			StreamCopier::copyStream(rs, cout);
			return true;
		}

		else
		{
			cout << "Server is down";
			cin.get();
			return false;
		}
	}
	
	catch(Exception e)
	{
		cout << "Something failed";
		cin.get();
	}
}

int main(int argc, char **argv)
{
	URI uri("https://query.yahooapis.com/v1/public/yql?q=select%20item.condition.text%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22buenosaires%2C%20tx%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys");
	std::string path(uri.getPathAndQuery());
	if (path.empty()) path = "/";

	HTTPClientSession session(uri.getHost(), uri.getPort());
	HTTPRequest request(HTTPRequest::HTTP_GET, path, HTTPMessage::HTTP_1_1);
	HTTPResponse response;

	if (!doRequest(session, request, response))
	{
		std::cerr << "Invalid username or password" << std::endl;
		return 1;
	}
}


